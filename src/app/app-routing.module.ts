import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CareersComponent } from './careers/careers.component';
import { HomepageComponent } from './homepage/homepage.component';
import { EachproductpageComponent } from './eachproductpage/eachproductpage.component';

const routes: Routes = [
  { path: "", component: HomepageComponent },
  { path: "careers", component: CareersComponent },
  { path: "product/:id", component: EachproductpageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [CareersComponent, HomepageComponent, EachproductpageComponent];
