import { Component } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-eachproductpage',
  templateUrl: './eachproductpage.component.html',
  styleUrls: ['./eachproductpage.component.css']
})
export class EachproductpageComponent {
  public idOFProductSelected = window.location.href.split("/").slice(4);
  productDetail: any = {};
  singleProduct: any = {};

  constructor(private _productData: ProductsService) {
    this._productData.products().subscribe((product) => {
      // console.log("Hema", product);
      // let k = product.filter((eachProduct) => {
      //   console.log("I am", eachProduct.id);
      //   // if (eachProduct.id === this.idOFProductSelected) {
      //   //   return eachProduct;
      //   // }
      // });
      // return this.productDetail = k;
      this.productDetail = product;
      this.singleProduct = this.productDetail[Number(this.idOFProductSelected) - 1];
      console.log(this.singleProduct);
    })
  }
}
