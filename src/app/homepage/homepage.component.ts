import { Component } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent {

  productList: any;
  loaderDisplay: boolean = true;
  loaderClass: string = "d-flex justify-content-center";
  allProductsContainerClass: string = "d-none";

  constructor(private _productData: ProductsService) {
    this._productData.products().subscribe((product) => {
      this.productList = product;
      this.loaderClass = "d-none"
      this.allProductsContainerClass = "allProductsContainer d-flex flex-wrap justify-content-center";
    });
  }
}
