import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { products } from './products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url = "https://fakestoreapi.com/products"

  constructor(private http: HttpClient) { }

  products() {
    return this.http.get<products[]>(this.url)
  }
}
