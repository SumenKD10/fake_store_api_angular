export interface products {
    title: string;
    image: string;
    price: number;
    category: string;
}